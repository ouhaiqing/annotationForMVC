package example;

import org.struts.annotation.Action;
import org.struts.annotation.ReqeustParam;
import org.struts.annotation.RequestMapping;



@Action
@RequestMapping("user")
public class LoginAction{

	
	@RequestMapping("login")
	public String excute(@ReqeustParam(value="username")Byte username,
			@ReqeustParam(value="password")String password) {
		System.out.println(username+" "+password);
		
		System.out.println("test for LoginAction");
		return "/view/success.jsp";
	}
	
	@RequestMapping("login2")
	public String excute(User user,@ReqeustParam(value="sign", required=true) String sign) {
		System.out.println(user.getUsername()+" "+user.getPassword());
		
		System.out.println("test for LoginAction:"+sign);
		return "/view/success.jsp";
	}

}
