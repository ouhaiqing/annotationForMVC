package org.struts.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.struts.constant.SystemConfig;
import org.struts.tools.MyRefecter;


public class ActionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException ,IOException {
		String urlString = this.getPath(request.getServletPath());
		
		@SuppressWarnings("unchecked")
//		this(当前对象)获取servlet上下文中的attribute
		Map<String, List<Object>> map = (Map<String, List<Object>>) this.getServletContext().getAttribute(SystemConfig.SERVLET_CONTEXT);
		
		List<Object> list = map.get(urlString);
		
		String forwardUrl = MyRefecter.doAction(list, request).toString();
		
//		获取转发器对象
		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardUrl);
		dispatcher.forward(request, response);
		
		
	};
	
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException ,IOException {
		this.doGet(req, resp);
	};
	
	
	private String getPath(String servletPath){
		return servletPath.split("\\.")[0];
	}

}
