package org.struts.tools;

import java.io.File;
import java.io.IOException;


import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;





public class XmlParser {
	public static final String XMLPATH = "resources/context-scan.xml";
	public XmlParser(){
		
	}
	
	public static String parseXML(String xmlPath){
		SAXBuilder builder = new SAXBuilder(); 
		String packageName="";
		try {
			Document document = builder.build(new File(xmlPath));
			Element e = document.getRootElement();
			Element element = e.getChild("context");
			packageName = element.getAttributeValue("package");
				
		} catch (JDOMException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		
		return packageName;
	}
	
	
	

	public static void main(String[] args) {
		XmlParser.parseXML(XMLPATH);
	}
}
