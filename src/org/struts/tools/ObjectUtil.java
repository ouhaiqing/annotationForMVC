package org.struts.tools;

public class ObjectUtil {
	
	public static Object getBaseValue(Class<?> c, String s){
		
		System.out.println(c.getName());
		if(c.getName().equals("java.lang.String")){
			return s;
		}else if(c.getName().equals("java.lang.Integer")){
			return StringToInteger(s);
		}else if(c.getName().equals("int")){
			return StringToInteger(s);
		}else if(c.getName().equals("java.lang.Short")){
			return StringToShort(s);
		}else if(c.getName().equals("short")){
			return StringToS(s);
		}else if(c.getName().equals("java.lang.Byte")){
			return StringToByte(s);
		}else if(c.getName().equals("byte")){
			return StringTobyte(s);
		}else if(c.getName().equals("java.lang.Float")){
			return StringToFloat(s);
		}else if(c.getName().equals("float")){
			return StringToF(s);
		}else if(c.getName().equals("java.lang.Double")){
			return StringToDouble(s);
		}else if(c.getName().equals("double")){
			return StringToD(s);
		}else if(c.getName().equals("java.lang.Boolean")){
			return StringToBoolean(s);
		}else if(c.getName().equals("boolean")){
			return StringToB(s);
		}else{
			return s;
		}
	}
	
	public static Object getDefaultValue(String baseType){
		if(baseType.equals("int")){
			return 0;
		}else if(baseType.equals("short")){
			return (short)0;
		}else if(baseType.equals("byte")){
			return (byte)0;
		}else if(baseType.equals("char")){
			return (char)0;
		}else if(baseType.equals("float")){
			return (float)0.0;
		}else if(baseType.equals("double")){
			return (double)0.0;
		}else if(baseType.equals("boolean")){
			return false;
		}
		return null;//防止传入的类型非基本类型， 故返回null
	}
	
	public static int StringToInt(String s){
		return Integer.valueOf(s);
	}
	public static Integer StringToInteger(String s){
		return Integer.valueOf(s);
	}
	
	public static byte StringTobyte(String s){
		return Byte.valueOf(s);
	}
	public static Byte StringToByte(String s){
		return Byte.valueOf(s);
	}
	
	public static short StringToS(String s){
		return Short.valueOf(s);
	}
	public static Short StringToShort(String s){
		return Short.valueOf(s);
	}
	
	public static float StringToF(String s){
		return Float.valueOf(s);
	}
	public static Float StringToFloat(String s){
		return Float.valueOf(s);
	}
	
	public static double StringToD(String s){
		return Double.valueOf(s);
	}
	public static Double StringToDouble(String s){
		return Double.valueOf(s);
	}
	
	
	public static boolean StringToB(String s){
		return Boolean.valueOf(s);
	}
	public static boolean StringToBoolean(String s){
		return Boolean.valueOf(s);
	}
	
	
	
}
