package org.struts.tools;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.struts.annotation.Action;
import org.struts.annotation.EntiyBean;
import org.struts.annotation.ReqeustParam;
import org.struts.annotation.RequestMapping;


public class MyRefecter {

	/**
	 * 解析包的时候保存先关注解信息和类信息
	 * 
	 * @param classList
	 *            类名
	 * @return
	 */
	public static Map<String, List<Object>> saveBean(List<String> classList) {
		Map<String, List<Object>> map = new HashMap<String, List<Object>>();
		for (String classPath : classList) {
			try {
				Class<?> clazz = Class.forName(classPath);
				if (clazz.getAnnotation(Action.class) != null) {
					System.out.println(classPath);
					String classMapping = null;
					if (clazz.getAnnotation(RequestMapping.class) != null) {
						RequestMapping requestMapping = clazz
								.getAnnotation(RequestMapping.class);
						classMapping = requestMapping.value();
					}

					Method[] methods = clazz.getDeclaredMethods();
					for (Method method : methods) {
						if (method.getAnnotation(RequestMapping.class) != null) {
							List<Object> list = new ArrayList<Object>();

							RequestMapping rm = method
									.getAnnotation(RequestMapping.class);
							list.add(classMapping != null ? "/" + classMapping
									+ "/" + rm.value() : "/" + rm.value());
							list.add(classPath);
							list.add(method.getName());
							list.add(method.getParameterTypes());
							map.put(list.get(0).toString(), list);
						}
					}

				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return map;
	}

	/**
	 * 获取url(Object类型到转字符串)
	 * 
	 * @param actionBean
	 * @return
	 */
	public static Object doAction(List<Object> list, HttpServletRequest request) {
		Object o = new Object();
		try {
			Class<?> clazz = Class.forName(list.get(1).toString());
			Object object = clazz.newInstance();
			Class<?>[] param = (Class[]) list.get(3);
			Method method = clazz.getMethod(list.get(2).toString(),
					(Class[]) list.get(3));

			Object[] paramObjects = new Object[param.length];
			// 组装方法参数
			System.out.println(param.length);
			
			paramObjects = MyRefecter.getPramAnnotaiton(method, request);
			o = method.invoke(object, paramObjects);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		}

		return o;
	}



	public static Object[] getPramAnnotaiton(Method method, HttpServletRequest request) {
		Class<?>[] param = method.getParameterTypes();
		Object[] objects = new Object[param.length];
		try {
			
			Annotation[][] reqeustParams = method.getParameterAnnotations();

			// 此处假设该参数只有一个注解或者没有
			int i = 0;
			for(Annotation[] params:reqeustParams){
				if(params.length==0){
					String baseType = param[i].getName();
					System.out.println(i+"  "+baseType);
					Object defaultValue = ObjectUtil.getDefaultValue(baseType);
					System.out.println(i+":jinru1:"+baseType+"	"+defaultValue);
					if(defaultValue!=null){
						objects[i] = defaultValue;
					}else{
						Class<?> clazz = Class.forName(param[i].getName());
						if (clazz.getAnnotation(EntiyBean.class) != null) {
							System.out.println("jinru2");
							Object co = clazz.newInstance();
							Field[] fields = clazz.getDeclaredFields();
							for (Field field : fields) {
								field.setAccessible(true);
								field.set(co, request.getParameter(field.getName()));
								field.setAccessible(false);
							}

							objects[i] = co;
						}else{
							//当然也可以判断下传入的特殊类， 比如request，response
							objects[i] = null;
						}
					}
				}else{
					for(Annotation aparam:params){
						System.out.println("jinru2");
						if (aparam instanceof ReqeustParam) {
							String paramName = ((ReqeustParam) aparam).value();
							System.out.println(((ReqeustParam) aparam).value() + "   "
									+ ((ReqeustParam) aparam).required());
							Object object = ObjectUtil.getBaseValue(param[i],request.getParameter(paramName));
							System.out.println(object.toString());
							objects[i] = object;
						}else{
							objects[i]=null;
						} 
					}
				}
				i++;
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objects;
	}

}
