package org.struts.tools;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;



public class MyPackageUtil {  
	  
    public static void main(String[] args) throws Exception {  
        String packageName = "example";  
        List<String> classNames = getAllClassName(packageName, true); 
        if (classNames != null) {  
            for (String className : classNames) {  
                System.out.println("className:"+className);  
            }  
        }  
    }  
  
   
	/**
	 * 获取某个包下的所有文件
	 * @param packageName
	 * @param childPackage（是否遍历子包）
	 * @return
	 */
	public static List<String> getAllClassName(String packageName, boolean childPackage){
    	List<String> fileNames = new ArrayList<String>(); 
    	ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    	if(packageName.endsWith("*"))
    		packageName = packageName.substring(0, packageName.lastIndexOf("."));
    	
    	String packagePath = packageName.replace(".", "/");
    	
    	URL url = classLoader.getResource(packagePath);
    	if(url!=null){
    		String filePath = url.getPath();
    		//为什么会出现空格不能正确解码呢？
    		getClassNameByFile(filePath.replaceAll("%20", " "),fileNames,childPackage);
    		
    	}
    	return fileNames;
	}
    
    public static List<String> getClassNameByFile(String filePath,List<String> fileNames, boolean childPackage){
    	//List<String> fileNames = new ArrayList<String>(); 
    	File[] files = new File(filePath).listFiles();	//获取该file下面一级的所有文件， 包括目录名和文件名
		for(File file:files){
			if(file.isDirectory() ){
				if(childPackage)
					getClassNameByFile(file.getPath(),fileNames,childPackage);
    		}else{
    			//注意来回的	'斜线'不一样
    			String fileName = file.getPath().substring(file.getPath().indexOf("\\classes")+9, file.getPath().lastIndexOf("."));
    			fileNames.add(fileName.replace("\\", ".")); 
    		}
		}
		return fileNames;
    }
    
}