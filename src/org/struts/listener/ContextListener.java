package org.struts.listener;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.struts.constant.SystemConfig;
import org.struts.tools.MyPackageUtil;
import org.struts.tools.MyRefecter;
import org.struts.tools.XmlParser;


public class ContextListener implements ServletContextListener{

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("信息：系统destroy");
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("信息：系统正在初始化");
		ServletContext servletContext =  arg0.getServletContext();
		
		String configPath = servletContext.getInitParameter(SystemConfig.CONTEXT_PATH);
		String tomcatPath = servletContext.getRealPath("\\");
		String packageUrl = XmlParser.parseXML(tomcatPath+configPath);
		List<String> packageList = MyPackageUtil.getAllClassName(packageUrl, true);
		Map<String, List<Object>> map = MyRefecter.saveBean(packageList);
		
		servletContext.setAttribute(SystemConfig.SERVLET_CONTEXT, map);
			
	}

}
